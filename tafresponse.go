package main

import (
	"encoding/xml"
	"fmt"
	"log"
	"net/http"
)

type TafResponse struct {
	XMLName                   xml.Name `xml:"response"`
	Text                      string   `xml:",chardata"`
	Xsd                       string   `xml:"xsd,attr"`
	Xsi                       string   `xml:"xsi,attr"`
	Version                   string   `xml:"version,attr"`
	NoNamespaceSchemaLocation string   `xml:"noNamespaceSchemaLocation,attr"`
	RequestIndex              string   `xml:"request_index"`
	DataSource                struct {
		Text string `xml:",chardata"`
		Name string `xml:"name,attr"`
	} `xml:"data_source"`
	Request struct {
		Text string `xml:",chardata"`
		Type string `xml:"type,attr"`
	} `xml:"request"`
	Errors      string `xml:"errors"`
	Warnings    string `xml:"warnings"`
	TimeTakenMs string `xml:"time_taken_ms"`
	Data        struct {
		Text       string `xml:",chardata"`
		NumResults string `xml:"num_results,attr"`
		TAF        struct {
			Text          string `xml:",chardata"`
			RawText       string `xml:"raw_text"`
			StationID     string `xml:"station_id"`
			IssueTime     string `xml:"issue_time"`
			BulletinTime  string `xml:"bulletin_time"`
			ValidTimeFrom string `xml:"valid_time_from"`
			ValidTimeTo   string `xml:"valid_time_to"`
			Latitude      string `xml:"latitude"`
			Longitude     string `xml:"longitude"`
			ElevationM    string `xml:"elevation_m"`
			Forecast      []struct {
				Text                string `xml:",chardata"`
				FcstTimeFrom        string `xml:"fcst_time_from"`
				FcstTimeTo          string `xml:"fcst_time_to"`
				WindDirDegrees      string `xml:"wind_dir_degrees"`
				WindSpeedKt         string `xml:"wind_speed_kt"`
				VisibilityStatuteMi string `xml:"visibility_statute_mi"`
				WxString            string `xml:"wx_string"`
				SkyCondition        []struct {
					Text           string `xml:",chardata"`
					SkyCover       string `xml:"sky_cover,attr"`
					CloudBaseFtAgl string `xml:"cloud_base_ft_agl,attr"`
					CloudType      string `xml:"cloud_type,attr"`
				} `xml:"sky_condition"`
				ChangeIndicator string `xml:"change_indicator"`
				WindGustKt      string `xml:"wind_gust_kt"`
				TimeBecoming    string `xml:"time_becoming"`
				Probability     string `xml:"probability"`
			} `xml:"forecast"`
		} `xml:"TAF"`
	} `xml:"data"`
}

func GetTaf(icao string) (TafResponse, error) {
	res, err := http.Get(fmt.Sprintf("https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=tafs&requestType=retrieve&format=xml&hoursBeforeNow=4&timeType=issue&mostRecent=true&stationString=%s", icao))
	if err != nil {
		panic(err)
	} else {
		fmt.Println("Successfully got an XML file.")
	}
	defer res.Body.Close()

	dec := xml.NewDecoder(res.Body)
	//fmt.Println(dec)
	var doc TafResponse
	if err := dec.Decode(&doc); err != nil {
		log.Fatal(err)
	}
	return doc, nil
}
