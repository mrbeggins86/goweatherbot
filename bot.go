package main

import (
	"fmt"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func main() {
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_APITOKEN"))
	if err != nil {
		fmt.Printf("Error while starting Bot: %v", err)
	}

	bot.Debug = true

	updateConfig := tgbotapi.NewUpdate(0)
	updateConfig.Timeout = 30

	updates := bot.GetUpdatesChan(updateConfig)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		if update.Message.IsCommand() {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

			// Extract the command from the Message.
			switch update.Message.Command() {
			case "start":
				msg.Text = "Welcome to the Aviation Weather Bot. \nPlease send me an ICAO ID of an airport and I'll provide you with any info available."
			default:
				msg.Text = "To get weather information, please send me an airport ICAO tag."
			}

			if _, err := bot.Send(msg); err != nil {
				fmt.Printf("Error while sending message: %v", err)
			}
		} else {
			// Checking if a message looks like and ICAO code. Must be lenght of 4.
			if len(update.Message.Text) == 4 {
				metar, err := GetMetar(update.Message.Text)
				if err != nil {
					fmt.Printf("Error while getting METAR: %v", err)
				}
				taf, err := GetTaf(update.Message.Text)
				if err != nil {
					fmt.Printf("Error while getting TAF: %v", err)
				}
				//If METAR info is present for the provided ID - sending it back to a user. If it's not present - it's not a valid ICAO code.
				if metar.Data.METAR.StationID != "" {
					//Creating Sky Condition statement for METAR part, as there might be more than one.
					skyCondition := ""
					for _, condition := range metar.Data.METAR.SkyCondition {
						if condition.SkyCover == "CAVOK" {
							skyCondition = skyCondition + fmt.Sprintf("Clouds: %s\n", condition.SkyCover)
						} else {
							skyCondition = skyCondition + fmt.Sprintf("Clouds: %s, at %s ft AGL\n", condition.SkyCover, condition.CloudBaseFtAgl)
						}
					}

					skyForecast := ""
					for _, forecast := range taf.Data.TAF.Forecast {
						//Creating Sky Condition statement for TAF part, as there might be more than one or absent.
						tafSkyCondition := ""
						for _, condition := range forecast.SkyCondition {
							if condition.CloudBaseFtAgl != "" {
								tafSkyCondition = tafSkyCondition + fmt.Sprintf("Clouds: %s, at %s ft AGL\n", condition.SkyCover, condition.CloudBaseFtAgl)
							} else {
								tafSkyCondition = tafSkyCondition + fmt.Sprintf("Clouds: %s\n", condition.SkyCover)
							}
						}
						//Creating Wind Condition statement for TAF part.
						tafWindCondition := ""
						if forecast.WindDirDegrees != "" {
							if forecast.WindGustKt != "" {
								tafWindCondition = fmt.Sprintf("Wind: %s at %s knots, gusts %s knots\n", forecast.WindDirDegrees, forecast.WindSpeedKt, forecast.WindGustKt)
							} else {
								tafWindCondition = fmt.Sprintf("Wind: %s at %s knots\n", forecast.WindDirDegrees, forecast.WindSpeedKt)
							}
						}
						//Creating Visibility Condition statement for TAF part.
						tafVisibilityCondition := ""
						if forecast.VisibilityStatuteMi != "" {
							tafVisibilityCondition = fmt.Sprintf("Visibility: %s nm\n", forecast.VisibilityStatuteMi)
						}
						//Creating forecast depending on a change indicator.
						if forecast.ChangeIndicator == "" {
							skyForecast = skyForecast + fmt.Sprintf("\n\nForecast\nFrom: %s\nTo: %s\n%sVisibility: %s nm\n%s", forecast.FcstTimeFrom, forecast.FcstTimeTo, tafWindCondition, forecast.VisibilityStatuteMi, tafSkyCondition)
						}
						if forecast.ChangeIndicator == "TEMPO" {
							skyForecast = skyForecast + fmt.Sprintf("\nTemporary\nFrom: %s\nTo: %s\n%s%s%s", forecast.FcstTimeFrom, forecast.FcstTimeTo, tafWindCondition, tafVisibilityCondition, tafSkyCondition)
						}
						if forecast.ChangeIndicator == "BECMG" {
							skyForecast = skyForecast + fmt.Sprintf("\nTransition\nFrom: %s\nUntil: %s\n%s%s%s", forecast.FcstTimeFrom, forecast.FcstTimeTo, tafWindCondition, tafVisibilityCondition, tafSkyCondition)
						}
					}
					//Creating METAR and TAF reply
					reply := fmt.Sprintf("For %s \nRaw: %s \nObservation time: %s \nWind: %s at %s knots \nTemperature: %s C \nDew point: %s C \nVisability: %s nm \nQNH: %s inHg \nElevation: %s m \n%sFlight category: %s", metar.Data.METAR.StationID, metar.Data.METAR.RawText, metar.Data.METAR.ObservationTime, metar.Data.METAR.WindDirDegrees, metar.Data.METAR.WindSpeedKt, metar.Data.METAR.TempC, metar.Data.METAR.DewpointC, metar.Data.METAR.VisibilityStatuteMi, metar.Data.METAR.AltimInHg, metar.Data.METAR.ElevationM, skyCondition, metar.Data.METAR.FlightCategory)
					if taf.Data.TAF.StationID != "" {
						reply = reply + fmt.Sprintf("\n\nTAF\nIssued: %s\nValid from: %s\nValid to: %s", taf.Data.TAF.IssueTime, taf.Data.TAF.ValidTimeFrom, taf.Data.TAF.ValidTimeTo) + skyForecast
					} else {
						reply = reply + fmt.Sprintf("\n\nTAF\nNo Data Available")
					}
					msg := tgbotapi.NewMessage(update.Message.Chat.ID, reply)
					msg.ReplyToMessageID = update.Message.MessageID
					if _, err := bot.Send(msg); err != nil {
						fmt.Printf("Error while sending message: %v", err)
					}
				} else {
					msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Must be a valid ICAO airport ID.")
					msg.ReplyToMessageID = update.Message.MessageID
					if _, err := bot.Send(msg); err != nil {
						fmt.Printf("Error while sending message: %v", err)
					}
				}
			} else {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Must be a valid ICAO airport ID.")
				msg.ReplyToMessageID = update.Message.MessageID
				if _, err := bot.Send(msg); err != nil {
					fmt.Printf("Error while sending message: %v", err)
				}
			}

		}
	}
}
