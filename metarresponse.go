package main

import (
	"encoding/xml"
	"fmt"
	"log"
	"net/http"
)

type MetarResponse struct {
	XMLName                   xml.Name `xml:"response"`
	Text                      string   `xml:",chardata"`
	Xsd                       string   `xml:"xsd,attr"`
	Xsi                       string   `xml:"xsi,attr"`
	Version                   string   `xml:"version,attr"`
	NoNamespaceSchemaLocation string   `xml:"noNamespaceSchemaLocation,attr"`
	RequestIndex              string   `xml:"request_index"`
	DataSource                struct {
		Text string `xml:",chardata"`
		Name string `xml:"name,attr"`
	} `xml:"data_source"`
	Request struct {
		Text string `xml:",chardata"`
		Type string `xml:"type,attr"`
	} `xml:"request"`
	Errors      string `xml:"errors"`
	Warnings    string `xml:"warnings"`
	TimeTakenMs string `xml:"time_taken_ms"`
	Data        struct {
		Text       string `xml:",chardata"`
		NumResults string `xml:"num_results,attr"`
		METAR      struct {
			Text                string `xml:",chardata"`
			RawText             string `xml:"raw_text"`
			StationID           string `xml:"station_id"`
			ObservationTime     string `xml:"observation_time"`
			Latitude            string `xml:"latitude"`
			Longitude           string `xml:"longitude"`
			TempC               string `xml:"temp_c"`
			DewpointC           string `xml:"dewpoint_c"`
			WindDirDegrees      string `xml:"wind_dir_degrees"`
			WindSpeedKt         string `xml:"wind_speed_kt"`
			VisibilityStatuteMi string `xml:"visibility_statute_mi"`
			AltimInHg           string `xml:"altim_in_hg"`
			SkyCondition        []struct {
				Text           string `xml:",chardata"`
				SkyCover       string `xml:"sky_cover,attr"`
				CloudBaseFtAgl string `xml:"cloud_base_ft_agl,attr"`
			} `xml:"sky_condition"`
			FlightCategory string `xml:"flight_category"`
			MetarType      string `xml:"metar_type"`
			ElevationM     string `xml:"elevation_m"`
		} `xml:"METAR"`
	} `xml:"data"`
}

func GetMetar(icao string) (MetarResponse, error) {
	res, err := http.Get(fmt.Sprintf("https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&hoursBeforeNow=3&mostRecent=true&stationString=%s", icao))
	if err != nil {
		panic(err)
	} else {
		fmt.Println("Successfully got an XML file.")
	}
	defer res.Body.Close()

	dec := xml.NewDecoder(res.Body)
	//fmt.Println(dec)
	var doc MetarResponse
	if err := dec.Decode(&doc); err != nil {
		log.Fatal(err)
	}
	return doc, nil
}
