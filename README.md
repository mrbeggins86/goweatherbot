This is a telegram bot for getting METAR and TAF weather information for Aviation.



Installation:

Building an app with Go (Linux)

Install go (https://go.dev/doc/install)

    wget https://go.dev/dl/go1.19.linux-amd64.tar.gz

    tar -C /usr/local -xzf go1.19.linux-amd64.tar.gz

    export PATH=$PATH:/usr/local/go/bin

    go version

    rm -f go1.19.linux-amd64.tar.gz
    
Clone repository

    git clone https://gitlab.com/mrbeggins86/goweatherbot.git

    cd goweatherbot

Create ENV variable representing your api token. (You can get it from Botfather)

    export TELEGRAM_APITOKEN=Change_to_your_api_token

Build and run an app

    go mod download && go mod verify

    go build

    ./goWeatherBot



Building and running Docker image

Install Docker

    sudo apt update && sudo apt install docker.io -y

    docker --version

    git clone https://gitlab.com/mrbeggins86/goweatherbot.git

    cd goweatherbot

Build an image

    docker build --build-arg apiToken=Change_to_your_api_token -t bot:1.0 .

Run the image in detached mode

    docker run -d bot:1.0

Usage:

Start the bot with Telegram. 

Send ICAO code of an airport. Bot will send you respective METAR and TAF info.


