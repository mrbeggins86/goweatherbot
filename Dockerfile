FROM golang:alpine
WORKDIR /usr/src/app
ARG apiToken=default
ENV TELEGRAM_APITOKEN=$apiToken
COPY . .
RUN go mod download && go mod verify
RUN go build
CMD ["./goWeatherBot"]